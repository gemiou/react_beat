import React from 'react';
import API from '../../../../api/api';
import {Item} from '../Item'


import './Items.css';

class Items extends React.Component {
    state = {
        data: []
    };
    componentDidMount() {
        API.get(`items`)
            .then(res => {
                console.log(res);
                const data = res.data;
                this.setState({data});
            })
    }
    render() {
        return (

            <div className="ride__list">
                {this.state.data.map(rides =>
                    <Item key={rides.id}
                        id={rides.id}
                        total={rides.ride.total}
                        pickup={rides.ride.pickup}
                        created={rides.ride.created_at}
                        dropoff={rides.ride.dropoff}/>
                )}
            </div>
        )
    }
}

export default Items;


