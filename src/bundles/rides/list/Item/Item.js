import React from 'react';
import { Link } from 'react-router-dom';
import {withRouter} from 'react-router-dom';
import {formatDate} from '../../../common/helper/Helper'
import {formatPrice} from '../../../common/helper/Helper'
import './Item.css';

class Item extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.state = {
            id: null
        };
    }
    handleClick = (id) => {
        this.setState({ id: id });
        this.props.history.push({pathname:'/ride-details',query: {id: 7}})
    };

    render(){
        return (
            <div className="card">
                <div className="card__header">
                    <span className="card__header__left">{formatDate(this.props.created)}</span>​
                    <span className="card__header__right">
                    Total fare:{formatPrice(this.props.total)}
                </span>
                </div>
                <div className="card__body">
                    <p>{this.props.dropoff}</p>
                    <Link to={`/ride-details/${this.props.id}`}>
                        <button>View Details</button>
                    </Link>
                </div>
            </div>
        )

    }


}

export default withRouter (Item);
