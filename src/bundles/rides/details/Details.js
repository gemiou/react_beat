import React from 'react';
import API from '../../../api/api';
import './Details.css';
import { withRouter } from 'react-router-dom'
import { CardHeader } from '../../common/components/Card_header';
import { MapImage } from '../../common/components/mapImage';

class Details extends React.Component {

    state = {
        driver: {}
    };

    componentDidMount() {
        const { match: { params } } = this.props;
        API.get(`items/${params.id}`)
            .then( result => {
                    const data = result.data.ride;
                    console.log(data);
                    this.setState({driver:data});
                }
            )
            .catch(error => alert(error))
    }

    handleBack = () => {
        this.props.history.goBack()
    };

    _renderObject(){
            return (
                <div>
                    <CardHeader created_at={this.state.driver['pickup']} total={this.state.driver['total']}/>
                    <MapImage src={this.state.driver['map']} />
                </div>
            )
    }




    render () {

        return (
                <div className="card">
                    <div>
                        <button onClick={this.handleBack}>Go Back</button>
                    </div>
                    <div className='well'>
                        {this._renderObject()}
                    </div>
                </div>
        )
    }
}

export default withRouter(Details);
