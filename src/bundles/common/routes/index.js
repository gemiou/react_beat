import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import { Items } from '../../rides/list/items'
import { Details } from '../../rides/details'
import { NotFound } from '../components/404'

const Router = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/ride" component={Items}/>
            <Route exact path="/ride-details/:id" component={Details}/>
            <Route component={NotFound}/>
        </Switch>
    </BrowserRouter>
);

export default Router;
