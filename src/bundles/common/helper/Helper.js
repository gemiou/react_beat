export const formatDate = (date) => {
    let event = new Date(date);
    return event.toLocaleString('en-GB', { timeZone: 'UTC' });
};

export const formatPrice = (price) => {
    let priceFormat = price.replace('S/', '');
    const formatter = new Intl.NumberFormat('it-IT', {
        style: 'currency',
        currency: 'EUR'
    });
    return formatter.format(priceFormat);
};
