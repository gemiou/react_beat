import React from 'react';
import './Layout.css';
import Router from '../../routes'


const Layout = (props) => (
    <div className="container">
        <div className="container__content">
            {/*<div className="container__title"><p>{props.title}</p></div>*/}
            <div className="container__content__ride">
                <Router/>
            </div>
        </div>
    </div>
);

export default Layout;
