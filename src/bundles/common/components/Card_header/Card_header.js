import React from 'react';
import './Card_header.css';

const CardHeader = ({created_at,total}) => (
    <div className="container">
        <div className="container__content">
            {created_at}
            {total}
        </div>
    </div>
);

export default CardHeader;
