import React from 'react';
import './404.css';

const NotFound = () => (
    <div className="container">
        Not found
    </div>
);

export default NotFound;
