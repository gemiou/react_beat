import React from 'react';
import './MapImage.css';

const MapImage = ({src}) => (
    <div className="iron-image-container">
        <img src={src} alt="Logo" />
    </div>
);

export default MapImage;

