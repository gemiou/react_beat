import React, { Component } from 'react';
import { Layout } from './bundles/common/components/Layout'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Layout title="Ride details"/>
      </div>
    );
  }
}

export default App;
